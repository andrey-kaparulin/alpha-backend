FROM python:3.9-slim as build
RUN apt-get update
RUN apt-get install -y --no-install-recommends \
    build-essential gcc

WORKDIR /usr/app
RUN python -m venv /usr/app/venv
ENV PATH="/usr/app/venv/bin:$PATH"

COPY requirements.txt .
RUN pip install -r requirements.txt


FROM python:3.10-slim@sha256:03294ce865f48bf7818aa391a9a28463070bfa068739bce13b9f407da3f37c17
WORKDIR /usr/app
COPY --from=build /usr/app/venv ./venv
COPY . .

ENV PATH="/usr/app/venv/bin:$PATH"
CMD [ "uvicorn", "--host=0.0.0.0", "--port=8000", "app.main:app" ]
