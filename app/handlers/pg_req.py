CREATE_TEST_TABLE = """CREATE TABLE IF NOT EXISTS test_table (id SERIAL PRIMARY KEY, test_data TEXT);"""
SELECT_ALL_DATA = "SELECT * FROM test_table;"
SELECT_POLL_WITH_OPTIONS = """SELECT * FROM polls JOIN options ON polls.id = options.poll_id WHERE polls.id = %s;"""
INSERT_TEST_DATA = "INSERT INTO test_table (test_data) VALUES (%s);"


def create_table(connection):
    with connection:
        with connection.cursor() as cursor:
            cursor.execute(CREATE_TEST_TABLE)


def insert_data_to_table(connection, data_for_insert):
    with connection:
        with connection.cursor() as cursor:
            cursor.execute(INSERT_TEST_DATA, (data_for_insert,))


def get_all_data(connection):
    with connection:
        with connection.cursor() as cursor:
            cursor.execute(SELECT_ALL_DATA)
            return cursor.fetchall()
