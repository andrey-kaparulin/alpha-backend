def push_data_to_list(connection, redis_val):
    with connection:
        connection.lpush("test_list", redis_val)


def get_full_list_slice(connection):
    response = []
    with connection:
        b_strings_list = connection.lrange("test_list", 0, -1)
        for x in b_strings_list:
            response.append(x.decode("UTF-8"))
        return response
