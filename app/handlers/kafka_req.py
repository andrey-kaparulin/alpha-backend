from threading import Thread

topic_response = []


def write_to_topic(kafka_producer_connection, data):
    data = str.encode(data)
    kafka_producer_connection.send('test_topic', data)


def read_from_topic(kafka_consumer_connection):
    # infinity consumer loop in thread
    for message in kafka_consumer_connection:
        topic_response.append(message.value.decode('utf-8'))


def run_consumer_loop(kafka_consumer_connection):
    thread = Thread(target=read_from_topic, args=(kafka_consumer_connection,))
    thread.start()


def get_topic_data():
    return topic_response
