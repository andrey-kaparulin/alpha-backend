import uvicorn
import psycopg2
import redis
import os
from fastapi import FastAPI
from dotenv import load_dotenv
from kafka import KafkaProducer
from kafka import KafkaConsumer
from fastapi.middleware.cors import CORSMiddleware
from .core.middleware import LoggingMiddleware
from .core.settings import app_settings
from .handlers import pg_req, redis_req, kafka_req


# env vars
load_dotenv()

database_uri = os.environ["DATABASE_URI"]
redis_uri = os.environ["REDIS_HOST"]
kafka_host = os.environ["KAFKA_HOST"]
kafka_port = os.environ["KAFKA_PORT"]

# connectors
pg_connection = psycopg2.connect(database_uri)
redis_connection = redis.Redis(redis_uri)
kafka_producer_connection = KafkaProducer(bootstrap_servers=[f'{kafka_host}:{kafka_port}'])
kafka_consumer_connection = KafkaConsumer('test_topic', bootstrap_servers=[f'{kafka_host}:{kafka_port}'])
# prerequisites
kafka_req.run_consumer_loop(kafka_consumer_connection)
pg_req.create_table(pg_connection)


def get_application() -> FastAPI:
    application = FastAPI(
        title=app_settings.PROJECT_NAME,
        debug=app_settings.DEBUG,
        version=app_settings.VERSION
    )

    application.add_middleware(
        CORSMiddleware,
        allow_origins=app_settings.ALLOWED_HOSTS,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    return application


app = get_application()
app.middleware('http')(
    LoggingMiddleware()
)


# PostgreSQL ep
@app.get("/select_all_from_pg", tags=["PostgreSQL"])
async def select_all_from_pg() -> list:
    res = pg_req.get_all_data(pg_connection)
    return res


@app.post("/insert_to_pg", tags=["PostgreSQL"])
async def insert_to_pg(pg_val: str) -> str:
    pg_req.insert_data_to_table(pg_connection, pg_val)
    return f"value '{pg_val}' was added to the PostgreSQL"


# Redis ep
@app.get("/get_full_list_slice", tags=["Redis"])
async def get_full_list_slice() -> list:
    res = redis_req.get_full_list_slice(redis_connection)
    return res


@app.post("/push_data_to_list", tags=["Redis"])
async def push_to_redis_list(redis_val: str) -> str:
    redis_req.push_data_to_list(redis_connection, redis_val)
    return f"value '{redis_val}' was added to the Redis"


# Kafka ep
@app.get("/get_topic_data", tags=["Kafka"])
async def get_topic_data() -> list:
    res = kafka_req.get_topic_data()
    return res


@app.post("/write_to_kafka_topic", tags=["Kafka"])
async def write_to_kafka_topic(topic_data: str) -> str:
    kafka_req.write_to_topic(kafka_producer_connection, topic_data)
    return f"msg '{topic_data}' was added to the Kafka topic"


# health check ep
@app.get("/health", tags=['Health'])
async def health() -> str:
    return "good day"


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
